#!/bin/bash

logdata=$(<sample.log)

echo 'Efficiently print all users mentioned in the log:'
echo -e "$(echo "${logdata}" | cut -d ' ' -f2 | sort -u)"

echo
echo 'Efficiently print each event in the log exactly once regardless of timestamp or associated user:'
echo -e "$(echo "${logdata}" | cut -d ' ' -f3- | sort -u)"

echo
echo 'For each day (defined as midnight to midnight) and for each user, print the number of times each event occurred for each user:'

#make array of unique dates (excluding time) by day:
dates=$(echo "${logdata}" | cut -d ' ' -f1 | cut -c1-10 | sort -u)
for day in $dates; do
    echo "${day}:"
    echo "${logdata}" | awk "/^$day/{print}" | cut -d ' ' -f2- | sort | uniq -c
    echo
done


